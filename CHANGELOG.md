# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.0](https://gitlab.com/ifb-elixirfr/covid19/rtools4emergen/compare/v0.0.9...v0.1.0) (2023-07-12)


### Features

* added two functions to create the variants evolution graphs ([f543660](https://gitlab.com/ifb-elixirfr/covid19/rtools4emergen/commit/f5436602be2636cf5c784471009497f64f36b243))

## [0.0.9](https://gitlab.com/ifb-elixirfr/covid19/rtools4emergen/compare/v0.0.8...v0.0.9) (2023-07-12)


### Bug Fixes

* generate a newer version of the package manual and corrected some titles in some functions ([c293a0a](https://gitlab.com/ifb-elixirfr/covid19/rtools4emergen/commit/c293a0ae18efa52aca39a45fad5ada1077615ae7))

## [0.0.8](https://gitlab.com/ifb-elixirfr/covid19/rtools4emergen/compare/v0.0.7...v0.0.8) (2023-07-11)


### Bug Fixes

* correction of the package build ([dd0cdb4](https://gitlab.com/ifb-elixirfr/covid19/rtools4emergen/commit/dd0cdb486f3af23bbc3a9cf9db3a042f5b3424da))

## [0.0.7](https://gitlab.com/ifb-elixirfr/covid19/rtools4emergen/compare/v0.0.6...v0.0.7) (2023-05-17)


### Bug Fixes

* base url modified ([bff4acd](https://gitlab.com/ifb-elixirfr/covid19/rtools4emergen/commit/bff4acd63486b2a55711c0eb0f4bba0be389b172))
* graphs title ([5c3af98](https://gitlab.com/ifb-elixirfr/covid19/rtools4emergen/commit/5c3af985e2b1522e7e605180db2ccdf583c65f76))
* remove a displayed token from a function and add automatic release ([c3cdf2b](https://gitlab.com/ifb-elixirfr/covid19/rtools4emergen/commit/c3cdf2bf7944601778f65cb215353295f88ae1e7))
* two stages defined ([3cb818f](https://gitlab.com/ifb-elixirfr/covid19/rtools4emergen/commit/3cb818f9f21a468cedd0ec33f634fbe0028fc811))
