FROM rocker/geospatial

# create directory for the app user
RUN mkdir -p /home/EMERGEN/ExternalReports

RUN apt-get update && apt-get install -y \
    sudo \
    gdebi-core \
    pandoc \
    pandoc-citeproc \
    libcurl4-gnutls-dev \
    libcairo2-dev \
    libxt-dev \
    xtail \
    wget \
    gnupg2 \
    libxml2-dev \
    libssl-dev \
    libpq-dev \
    libv8-dev \
    default-jre \
    r-cran-rjava \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/ \
    && rm -rf /tmp/downloaded_packages/ /tmp/*.rds

RUN apt-get update

## Install packages for Dependencies
RUN Rscript -e "install.packages(c('sf', 'bookdown', 'tinytex',  'echarts4r', 'httr', 'jsonlite', 'dplyr', 'jsonlite', 'sp', 'raster', 'geojsonio', 'rmapshaper', 'DescTools'), repos='https://cran.rstudio.com/', dependencies = TRUE)" \
    && rm -rf /tmp/downloaded_packages/ /tmp/*.rds

## Install packages Rtools4Emergen
ARG INCUBATOR_VER=unknown
RUN Rscript -e "library(devtools) ; install_gitlab('ifb-elixirfr/covid19/rtools4emergen')" \
    && rm -rf /tmp/downloaded_packages/ /tmp/*.rds

WORKDIR /home/EMERGEN/Reports
