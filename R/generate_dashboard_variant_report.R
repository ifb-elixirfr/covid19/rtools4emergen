#' @title Generate dashboard variant report
#' @author Thomas Denecker, \email{Thomas.Denecker@@france-bioinformatique.fr}
#' @description Dashboard variant report
#' @param base_url EMERGEN-DB URL. By default = 'https://emergen-db.france-bioinformatique.fr/fr/'
#' @param emergenToken EMERGEN-DB Token
#' @param definition Variant definition
#' @param typeVariant Variant type : lineage or clade
#' @import echarts4r
#' @import httr
#' @import jsonlite
#' @import dplyr
#' @import reshape
#' @import stringr
#' @import RColorBrewer
#' @import sp
#' @import raster
#' @import geojsonio
#' @import rmapshaper
#' @import DescTools
#' @export
generate_dashboard_variant_report <- function(
    base_url = 'https://emergen-db.france-bioinformatique.fr/fr/',
    emergenToken,
    definition,
    typeVariant
    ){

  si <- sessionInfo()

  paramsList <- list(
    si = si,
    emergenToken_params = emergenToken,
    workdir_params =  getwd(),
    base_url = base_url,
    definition = definition,
    typeVariant = typeVariant
  )

  output_dir <- getwd()

  report_path <- tempfile(fileext = ".Rmd")
  tmp_path <- dirname(report_path)

  file <- system.file("Rmd/dashboard_variant.Rmd", package = 'Rtools4Emergen')
  file.copy(file, tmp_path, overwrite = TRUE)

  folder <- system.file("Rmd/html", package = 'Rtools4Emergen')
  file.copy(folder, tmp_path, recursive = TRUE, overwrite = TRUE)


  rmarkdown::render(paste0(tmp_path,"/dashboard_variant.Rmd"),
                    params = paramsList,
                    output_dir = output_dir)
}
