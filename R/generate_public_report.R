#' @title Create public chart : Daily Sampling Status
#' @author Thomas Denecker, \email{Thomas.Denecker@@france-bioinformatique.fr}
#' @author Chiara Antoinat, \email{chiara.antoinat@@gmail.com}
#' @description Create public chart : Daily Sampling Status
#' @param base_url EMERGEN-DB URL. By default = 'https://emergen-db.france-bioinformatique.fr/'
#' @param dynamic Plot dynamic (TRUE/FALSE). By default, True
#' @import httr
#' @import jsonlite
#' @import ggthemes
#' @import ggplot2
#' @import ggthemes
#' @import plotly
#' @import tidyverse
#' @import ragg
#' @import lubridate
#' @import webshot
#' @import tinytex
#' @import googleVis
#' @export
generate_public_report <- function(
  base_url = 'https://emergen-db.france-bioinformatique.fr/fr/'){

  si <- sessionInfo()

  paramsList <- list(
    si = si
  )

  output_dir <- getwd()

  report_path <- tempfile(fileext = ".Rmd")
  tmp_path <- dirname(report_path)

  file <- system.file("Rmd/public_stats.Rmd", package = 'Rtools4Emergen')
  file.copy(file, tmp_path, overwrite = TRUE)

  folder <- system.file("Rmd/html", package = 'Rtools4Emergen')
  file.copy(folder, tmp_path, recursive = TRUE, overwrite = TRUE)

  rmarkdown::render(paste0(tmp_path,"/public_stats.Rmd"),
                    params = paramsList,
                    output_dir = output_dir)
}
