#' @title Visualisation du nombre de séquences produites par semaine et par indication
#' @author Imane Messak, \email{Imane.Messak@@france-bioinformatique.fr}
#' @description Nombre de séquences produites par semaine et par indication
#' @param base_url EMERGEN-DB URL. By default = 'https://emergen-db.france-bioinformatique.fr/fr/'
#' @param emergenToken EMERGEN-DB Token
#' @return echarts
#' @import echarts4r
#' @import httr
#' @import jsonlite
#' @import dplyr
#' @import reshape
#' @import stringr
#' @import RColorBrewer
#' @export
overview_labo <- function(
  # base_url = 'https://emergen-db.france-bioinformatique.fr/fr/',
  base_url = 'http://localhost:8000/en/',
  emergenToken, lab_name=NA) {

  message("API call from EMERGEN-DB")
  message("\tBase URL: ", base_url)

  # Entry point to get all the results in JSON format
  url = paste0(base_url, paste0("api/stats/labo/dashboard/overview/",lab_name,"/"))
  res= GET(URLencode(url), add_headers(Authorization = paste("Token", emergenToken,sep = " ")))

  if(res$status_code != 200) {
    message("\n\tError API\n")
    stop(paste("Status code:", res$status_code))
  }

  # Collecting data at JSON format
  json_object = jsonlite::prettify(rawToChar(res$content))

  # Conversion to the list format
  R_list_object = fromJSON(json_object)

  return(R_list_object)
}

