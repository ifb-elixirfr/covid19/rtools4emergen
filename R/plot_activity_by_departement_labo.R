#' @title Visualisation de l'activité de séquençage par département
#' @author Thomas Denecker, \email{Thomas.Denecker@@france-bioinformatique.fr}
#' @author Imane Messak, \email{Imane.Messak@@france-bioinformatique.fr}
#' @description Activité de séquençage par département
#' @param base_url EMERGEN-DB URL. By default = 'https://emergen-db.france-bioinformatique.fr/fr/'
#' @param emergenToken EMERGEN-DB Token
#' @return echarts
#' @import echarts4r
#' @import httr
#' @import jsonlite
#' @import dplyr
#' @import reshape
#' @import stringr
#' @import sp
#' @import raster
#' @import geojsonio
#' @import rmapshaper
#' @export
plot_activity_by_departement_labo <- function(
  base_url = 'https://emergen-db.france-bioinformatique.fr/fr/',
  emergenToken, lab_name) {

  message("API call from EMERGEN-DB")
  message("\tBase URL: ", base_url)

  # Entry point to get all the results in JSON format
  url = paste0(base_url, paste0("api/stats/labo/dashboard/map/", lab_name, "/departement/"))
  res= GET(URLencode(url), add_headers(Authorization = paste("Token", emergenToken,sep = " ")))

  if(res$status_code != 200) {
    message("\n\tError API\n")
    stop(paste("Status code:", res$status_code))
  }

  # Collecting data at JSON format
  json_object = jsonlite::prettify(rawToChar(res$content))

  # Conversion to the list format
  R_list_object = fromJSON(json_object)

  # data preparation
  dataTemp = R_list_object$data

  colnamesTable = NULL
  for(i in R_list_object$columns_name){
    if(nchar(i)== 9){
      temp = unlist(str_split(i, "S"))
      colnamesTable = c(colnamesTable,
                        paste0(temp[1], "S0", temp[2]))
    } else {
      colnamesTable = c(colnamesTable, i)
    }
  }

  colnames(dataTemp) = colnamesTable
  # dataTemp = dataTemp[ , sort(colnamesTable)]
  dataTemp <- dataTemp %>%
    as_tibble() %>%
    setNames(colnames(dataTemp)) %>%
    dplyr::select(order(colnames(dataTemp)))
  dataTemp <- as.data.frame(dataTemp)
  dataTemp <- melt(dataTemp, id=c("code","name"))
  dataTemp$value <- as.numeric(dataTemp$value)

  # Get geoJson data
  france_sp <- raster::getData('GADM', country = 'France', level = 2)
  france_small <- rmapshaper::ms_simplify(france_sp, keep = 0.05)
  france_json_small <- geojsonio::geojson_list(france_small)

  guadeloupe_sp <- raster::getData('GADM', country = 'Guadeloupe', level = 0)
  guadeloupe_small <- rmapshaper::ms_simplify(guadeloupe_sp, keep = 0.05)
  guadeloupe_json_small <- geojsonio::geojson_list(guadeloupe_small)

  martinique_sp <- raster::getData('GADM', country = 'Martinique', level = 0)
  martinique_small <- rmapshaper::ms_simplify(martinique_sp, keep = 0.05)
  martinique_json_small <- geojsonio::geojson_list(martinique_small)

  guyane_sp <- raster::getData('GADM', country = 'French Guiana', level = 0)
  guyane_sp$NAME_0 = "Guyane"
  guyane_small <- rmapshaper::ms_simplify(guyane_sp, keep = 0.05)
  guyane_json_small <- geojsonio::geojson_list(guyane_small)

  mayotte_sp <- raster::getData('GADM', country = 'Mayotte', level = 0)
  mayotte_small <- rmapshaper::ms_simplify(mayotte_sp, keep = 0.05)
  mayotte_json_small <- geojsonio::geojson_list(mayotte_small)

  reunion_sp <- raster::getData('GADM', country = 'Réunion', level = 0)
  reunion_sp$NAME_0 = "La Réunion"
  reunion_small <- rmapshaper::ms_simplify(reunion_sp, keep = 0.05)
  reunion_json_small <- geojsonio::geojson_list(reunion_small)

  titles = list()
  for (i in as.character(unique(dataTemp$variable))){
    titles = c(titles, list(list(text = paste0("France", " - ", i))))
  }

  # plot
  echarts <- dataTemp |>
    group_by(variable) |>
    e_charts(name, timeline = TRUE) |>
    e_tooltip(trigger = "item") |>
    e_map_register("France", france_json_small) |>
    e_map_register("Guadeloupe", guadeloupe_json_small) |>
    e_map_register("Martinique", martinique_json_small) |>
    e_map_register("Mayotte", mayotte_json_small) |>
    e_map_register("La Réunion", reunion_json_small) |>
    e_map_register("Guyane", guyane_json_small) |>
    e_map(value, map = "France",name = "Prélèvements envoyés pour séquençage",
          nameProperty = "NAME_2") |>
    e_map(value, map = "Guadeloupe",name = "Prélèvements envoyés pour séquençage",
          nameProperty = "NAME_0",
          bottom='80%', left="3%", top="5%",
          label = list()) |>
    e_map(value, map = "Martinique",name = "Prélèvements envoyés pour séquençage",
          nameProperty = "NAME_0",
          bottom='61%', left="3%", top="24%",
          label = list()) |>
    e_map(value, map = "La Réunion",name = "Prélèvements envoyés pour séquençage",
          nameProperty = "NAME_0",
          bottom='42%', left="3%", top="43%",
          label = list()) |>
    e_map(value, map = "Mayotte",name = "Prélèvements envoyés pour séquençage",
          nameProperty = "NAME_0",
          bottom='24%', left="5%", top="61%",
          label = list()) |>
    e_map(value, map = "Guyane",name = "Prélèvements envoyés pour séquençage",
          nameProperty = "NAME_0",
          bottom='5%', left="5%", top="80%",
          label = list()) |>
    e_visual_map( min = 0, max = max(dataTemp$value[dataTemp$value != max(dataTemp$value)])  , calculable=FALSE,
                  text = list(max(dataTemp$value[dataTemp$value != max(dataTemp$value)]), 0),
                  range= list(1, max(dataTemp$value[dataTemp$value != max(dataTemp$value)]) ), left= 'right') |>
    e_timeline_serie(
      title = titles
    ) |>
    e_title(left= 'center') |>
    e_timeline_opts(
      currentIndex = (length(as.character(unique(dataTemp$variable))) - 1),
    )|>
    e_toolbox_feature(feature = "saveAsImage") |>
    e_toolbox_feature(feature = "dataView", readOnly = TRUE)

  return(echarts)
}

