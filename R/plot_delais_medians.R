#' @title Visualisation des délais médians de séquençage
#' @author Thomas Denecker, \email{Thomas.Denecker@@france-bioinformatique.fr}
#' @author Imane Messak, \email{Imane.Messak@@france-bioinformatique.fr}
#' @description Délais médians de séquençage par type de laboratoires (plateformes CNR, plateformes AMI, réseau ANRS|MIE et réseau LBM|ARS) par semaine de réception du prélèvement
#' @param base_url EMERGEN-DB URL. By default = 'https://emergen-db.france-bioinformatique.fr/fr/'
#' @param emergenToken EMERGEN-DB Token
#' @return echarts
#' @import echarts4r
#' @import httr
#' @import jsonlite
#' @import dplyr
#' @import reshape
#' @import stringr
#' @import RColorBrewer
#' @export
plot_delais_medians <- function(
    base_url = 'https://emergen-db.france-bioinformatique.fr/fr/',
    emergenToken) {

  message("API call from EMERGEN-DB")
  message("\tBase URL: ", base_url)

  # Entry point to get all the results in JSON format
  url = paste0(base_url, "api/stats/delai/median/by/week/and/lab/fig/")
  res= GET(url, add_headers(Authorization = paste("Token", emergenToken ,sep = " ")))

  if(res$status_code != 200) {
    message("\n\tError API\n")
    stop(paste("Status code:", res$status_code))
  }

  # Collecting data at JSON format
  json_object = jsonlite::prettify(rawToChar(res$content))

  # Conversion to the list format
  R_list_object = fromJSON(json_object)

  # data preparation
  dataTemp = NULL
  for( i in 1:nrow(R_list_object$data2)){
    dataTemp = rbind.data.frame(dataTemp, cbind(R_list_object$xAxis,
                                                as.numeric(unlist(R_list_object$data2[i,"y"])),
                                                rep(R_list_object$data2[i,"name"], length(R_list_object$xAxis))
    ))
  }

  colnames(dataTemp) = c("x", "y", "Labo")
  dataTemp$y = as.numeric(as.character(dataTemp$y))
  dataTemp$y [dataTemp$y ==0 ] <- NA
  dataTemp$x <- factor(dataTemp$x, levels = R_list_object$xAxis)

  # plot
  echarts <- dataTemp |>
    group_by(Labo) |>
    e_charts(x) |>
    e_line(y) |>
    e_legend(top = "5%") |>
    e_tooltip(trigger = "axis") |>
    e_datazoom(x_index = 0, type = "slider", bottom = "3%") |>
    e_toolbox_feature(feature = "saveAsImage") |>
    e_toolbox_feature(feature = "dataView", readOnly = TRUE) |>
    e_x_axis(name="Semaine de réception",
             nameLocation = "middle", nameGap= 25) |>
    e_grid(bottom = "17%") |>
    e_y_axis(name="Délais médians (en jour)", nameLocation = 'middle',
             nameGap= 50) |>
    e_title(text = "Délais médians de séquençage par type de laboratoire",
            textStyle = list(fontSize = list(14)))

  return(echarts)
}

