#' @title Birth year fill rate for a specific sequencing lab
#' @description Visualisation of the birth year fill rate for a specific sequencing lab
#' @author Anliat Mohamed,  \email{anliat.mohamed@@france-bioinformatique.fr}
#' @param base_url EMERGEN-DB URL. By default = 'https://emergen-db.france-bioinformatique.fr/fr/'
#' @param emergenToken EMERGEN-DB Token
#' @param lab_name Name of the sequencing lab
#' @param year_filter filter the result according to the results year
#' @param quality_filter the metadata
#' @import echarts4r
#' @import httr
#' @import dplyr
#' @import reshape
#' @import stringr
#' @import RColorBrewer
#' @export
plot_fill_rate_birth_year_labo <- function(
    base_url,emergenToken,
    lab_name, quality_filter, year_filter) {

  message("API call from EMERGEN-DB")
  message("\tBase URL: ", base_url)

  # Entry point to get all the results in JSON format
  url = paste0(base_url, paste0("api/stats/metadata/quality/labo/",
                                lab_name,"/",
                                quality_filter,"/",
                                year_filter,"/"))
  res= GET(URLencode(url), add_headers(Authorization = paste("Token",
                                                             emergenToken,
                                                             sep = " ")))

  if(res$status_code != 200) {
    message("\n\tError API\n")
    return(cat("Pas de données disponible pour l'année", year_filter))
  }

  # Collecting data at JSON format
  json_object = jsonlite::prettify(rawToChar(res$content))

  # Conversion to the list format
  R_list_object = fromJSON(json_object)

  # data preparation
  dataTemp <- data.frame(Semaine = R_list_object$Semaine,
                         anneeNaissance = R_list_object$anneeNaissance)

  # Function to caluclate the birth_year fill rate
  dateFun <- function(subTable){
    n <- nrow(subTable)
    collected = nrow(subset(subTable, !is.na(anneeNaissance)))
    not_collected = nrow(subset(subTable, is.na(anneeNaissance)))
    dt <- rbind.data.frame(c(collected/n*100, not_collected/n*100))
    colnames(dt) <- c("Collected", "Not collected")
    return(dt)
  }

  # Gather the data by week and calculate the birth_year fill rate for each week
  dataDate <- reshape2::melt(dataTemp %>%
                               group_by(Semaine) %>%
                               group_map(~dateFun(.x)) %>%
                               setNames(unique(sort(dataTemp$Semaine))))

  # Echart graph
  echarts <- dataDate |>
    group_by(variable) |>
    e_charts(L1) |>
    e_bar(value, stack = "total") |>
    e_legend(left= 'center', top = "5%") |>
    e_tooltip(trigger = "axis", axisPointer=list(type='shadow')) |>
    e_toolbox_feature(feature = "saveAsImage", name = list('Percentage of collected birth_year')) |>
    e_x_axis(name="Semaine de résultat de séquençage", nameLocation = "middle", axisLabel=list(interval=0, rotate=45), nameGap= 60) |>
    e_grid(bottom = "15%") |>
    e_title(paste0("Taux de remplissage de la date de naissance: ", lab_name),
            textStyle = list(fontSize = list(14))) |>
    e_color(c("#91CC75", "#EE6766"))|>
    e_y_axis(name="%", nameLocation = 'middle', nameGap= 40, max=100)

  return(echarts)
}
