
#' @title plot a Hinton diagram
#' @author Jacques van Helden, \email{Jacques.van-Helden@@france-bioinformatique.fr}
#' @description Hinton diagrams provide an intuitive view of a matrix
#' where each cell each represented by a circle with an area proportional to its value.
#' Distinct colors are used for positive and negative
#' values.
#' @param x A matrix or data frame with the values to be plotted
#' @param line.colors=c("black","grey") line colors for circles associated to positive and negative value, resp.
#' @param fill.colors=c("black","white")  fill colors for circles associated to positive and negative value, resp.
#' @param scale=1 Factor for scaling the radius of the circles
#' @param axes=c(1,2) Locations where axis legends have to be plotted. Same numbering as the function axis(): 1=below, 2=left, 3=above and 4=right.
#' @param las=2 Orientation of the axis labels. Passed to axis(). Same numbering as the function axis(): 1=below, 2=left, 3=above and 4=right.
#' @param shape="circle" supported: circle | square | rectangle | bars
#' @param return.result = F If true, a detailed result is returned as a data frame
#' @param xlab=NA,
#' @param ylab=NA By default, no labels on the axes
#' @param ... Additional parameters are passed to the plot function symbols()
#' @export
plot_hinton <- function (x, ## A matrix or data frame with the values to be plotted
                         line.colors=c("black", "grey"), ## line colors for circles associated to positive and negative value, resp.
                         fill.colors=c("black", "white"), ## fill colors for circles associated to positive and negative value, resp.
                         scale=1, ## Factor for scaling the radius of the circles
                         axes=c(1, 2), ## Locations where axis legends have to be plotted.
                         las = 2, ## Orientation of the axis labels. Passed to axis()
                         ## Same numbering as the function axis(): 1=below, 2=left, 3=above and 4=right.
                         shape="circle", ## supported: circle | square | rectangle | bars
                         return.result = F, ## If true, a detailed result is returned as a data frame
                         xlab=NA,
                         ylab=NA, ## By default, no labels on the axes
                         ... ## Additional parameters are passed to the plot function symbols()
) {

  ## Reverse the order ot the columns in order to o from top to bottom rather than from bottom to top
  x <- x[nrow(x):1, ]

  ## Compute the number of columns and X, Y positions of the circles
  nr <- nrow(x)
  nc <- ncol(x)
  x.centers <- ((1:nc)-0.5) / nc
  y.centers <- ((1:nr)-0.5) / nr

  ## Cast data frame of values to a vector
  value <- as.vector(as.matrix(t(x)))


  ## Define sign-specific colors
  x.sign <- sign(value) + 1
  x.sign[x.sign == 2] <- 1
  circle.line.colors <- line.colors[2 - x.sign]
  circle.fill.colors <- fill.colors[2 - x.sign]

  ## Compute a vector of X and Y coordinates for the circles
  centers <- expand.grid(x.centers, y.centers)
  names(centers) <- c("x", "y")

  ## Rectangle sizes
  height <- scale * sqrt(abs(value)) / (max(sqrt(abs(value))) * nr)
  width <- scale * sqrt(abs(value)) / (max(sqrt(abs(value))) * nc)
  rect <- as.matrix(data.frame(width, height))
  min.side <- apply(rect, 1, min)
  squares <- as.matrix(data.frame(min.side, min.side))

  ## Bar sizes
  if (shape == "bars") {
    bar.height <- scale * abs(value) / (max(abs(value)) * nr)
    bar.width <- scale / nc
    rect <- as.matrix(data.frame(bar.width, bar.height))
  }

  ## Inch dimensions (necessary for circle)
  din <- data.frame(width * par("din")[1], height * par("din")[2])
  names(din) <- c("width", "height")
  side.inches <- apply(din, 1, min) ## For squares, the function symbol() uses the X dimensions

  ## Draw the circles
  if (shape == "circle") {
    symbols(x = centers[, 1], y = centers[, 2],
            fg = circle.line.colors, bg = circle.fill.colors,
            new = F, xlab = xlab, ylab = ylab, xaxt = "n", yaxt = "n", inches = max(side.inches / 2), circles = side.inches / 2, ...)

  } else if (shape == "square") {
    symbols(x = centers[, 1], y = centers[, 2],
            fg = circle.line.colors, bg = circle.fill.colors,
            new = F, xlab = xlab, ylab = ylab, xaxt = "n", yaxt = "n", inches = max(side.inches), squares = side.inches, ...)
    ## The square function is tricky, I simply use a rectangle with square coordinates
    #    symbols(x = centers[, 1], y = centers[, 2], fg = circle.line.colors, bg = circle.fill.colors, new = F, xlab = xlab, ylab = ylab, xaxt = "n", yaxt = "n", inches = F, rectangles = squares, ...)

  } else if ((shape == "rectangle") || (shape == "bars")){
    symbols(x = centers[, 1], y = centers[, 2],
            fg = circle.line.colors, bg = circle.fill.colors,
            new = F, xlab = xlab, ylab = ylab, xaxt = "n", yaxt = "n", inches = F, rectangles = rect, ...)

  } else {
    stop('Invalid shape for plot.hinton.diagram(): only "circle" and "square" are supported.')
  }

  ## Draw the axes
  if (1 %in% axes) { axis(1, labels = names(x), at = x.centers, las = las) }
  if (2 %in% axes) { axis(2, labels = row.names(x), at = y.centers, las = las) }
  if (3 %in% axes) { axis(3, labels = names(x), at = x.centers, las = las) }
  if (4 %in% axes) { axis(4, labels = row.names(x), at = y.centers, las = las) }

  ## Compute a data frame with the results if required
  if (return.result) {
    result <- data.frame(value = value,
                         centers = centers,
                         rect = rect,
                         din = din,
                         side.inches = side.inches,
                         line.col = circle.line.colors,
                         fill.col = circle.fill.colors
    )
    return(result)
  }
}
