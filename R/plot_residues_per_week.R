#' @title Plot profiles of residue frequencies per sampling week
#' @author Jacques van Helden, \email{Jacques.van-Helden@@france-bioinformatique.fr}
#' @param lab
#' @param anonymize=FALSE if TRUE, replace lab ame by a pseudo
#' @export
plot_residues_per_week <- function(lab,
                                   anonymize = FALSE) {

  par(mfrow = c(nrow(gene_var_pos), 2))
  par(mar = c(7, 5, 6, 3))

  for (i in 1:nrow(gene_var_pos)) {
    gene <- gene_var_pos[i, "gene"]
    variant <- gene_var_pos[i, "var"]
    position <- gene_var_pos[i, "pos"]
    residue_column <- paste(sep = ":", gene, position)
    # cat("\n\n###", variant)
    selected_samples <- subset(seq_table,
                               (Nom.du.laboratoire.sequenceur == lab)
                               & (greek_letter == variant))


    n <- nrow(selected_samples)
    message("\t\t",  variant, "\t", n, "\tsamples for lab ", lab, "\t")
    if (n > 0) {
      occ <- table(selected_samples[, c("result_year_week", residue_column)])

      ## Add the number of samples to the rownames
      selected_samples_per_week <- table(selected_samples$result_year_week)
      rownames(occ) <- paste0(rownames(occ),
                              " (n=", selected_samples_per_week[rownames(occ)], ")")

      pc <- 100 * occ / apply(occ, 1, sum)
      # colnames(pc)


      if (anonymize) {
        main <- paste0(lab_labels[lab], "\n", residue_column, " in ", variant, " (n=X)")
      } else {
        main <- paste0(lab, "\n", residue_column, " in ", variant, " (n=", n, ")")
      }

      if (nrow(pc) > 1) {

        barplot(
          t(occ),
          main = main,
          col = residue_colors[colnames(occ)],
          las = 2, cex.names = 0.8)
        legend("topleft", cex = 0.8,
               legend = colnames(occ) ,
               fill = residue_colors[colnames(occ)])
      }

      barplot(
        t(pc),
        main = main,
        ylim = c(0,100), las = 2, cex.names = 0.8,
        legend.text = colnames(pc) ,
        col = residue_colors[colnames(pc)])
    }
  }
  par(par_ori)

}
