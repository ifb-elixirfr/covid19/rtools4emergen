#' @title Statistics
#' @author Imane Messak, \email{Imane.Messak@@france-bioinformatique.fr}
#' @description Variants Evolution
#' @param base_url EMERGEN-DB URL. By default = 'https://emergen-db.france-bioinformatique.fr/fr/'
#' @param emergenToken EMERGEN-DB Token
#' @return echarts
#' @import echarts4r
#' @import httr
#' @import jsonlite
#' @import dplyr
#' @import reshape
#' @import stringr
#' @import RColorBrewer
#' @export
plot_samples_per_variant_per_week <- function(
    base_url = 'https://emergen-db.france-bioinformatique.fr/fr/',
    emergenToken) {

  message("API call from EMERGEN-DB")
  message("\tBase URL: ", base_url)

  # Entry point to get all the results in JSON format
  url = paste0(base_url, "api/stats/variant/evolution/fig/")
  res= GET(url, add_headers(Authorization = paste("Token", emergenToken,sep = " ")))

  if(res$status_code != 200) {
    message("\n\tError API\n")
    stop(paste("Status code:", res$status_code))
  }

  # Collecting data at JSON format
  json_object = jsonlite::prettify(rawToChar(res$content))

  # Conversion to the list format
  R_list_object = fromJSON(json_object)

  # data preparation
  dataTemp = NULL
  for( i in 1:nrow(R_list_object$data)){
    dataTemp = rbind.data.frame(dataTemp, cbind(R_list_object$xAxis,
                                                as.numeric(unlist(R_list_object$data[i,"y"])),
                                                rep(R_list_object$data[i,"name"], length(R_list_object$xAxis))
    ))
  }

  colnames(dataTemp) = c("x", "y", "variant")
  dataTemp$y = as.numeric(as.character(dataTemp$y))
  dataTemp$x <- factor(dataTemp$x, levels = R_list_object$xAxis)

  # List of valid variants
  valid_variants <- c("Alpha", "Beta", "Gamma", "Delta", "Omicron")

  # Replace values not in valid_variants with "other variants"
  dataTemp <- dataTemp %>%
    mutate(variant = ifelse(variant %in% valid_variants, variant, "Other variants"))

  # Stacking information
  stacking_info <- dataTemp %>%
    group_by(x, variant) %>%
    summarise(y = sum(y), .groups = "keep")

  # plot
  echarts <- stacking_info |>
    group_by(variant) |>
    e_charts(x) |>
    e_bar(y, stack = "grp") |>
    e_legend(top = "4%") |>
    e_color(c('#5470c6', '#91cc75', '#f1c232', '#EE6765',
              '#72c0de', '#FC8452')) |>
    e_animation(show = FALSE,
                threshold = 0,
                duration = 0,
                easing = 0,
                delay = 0) |>
    e_tooltip(trigger = "axis", formatter = htmlwidgets::JS('
                                                          function (params) {
            let tooltip = `<span>${params[0].axisValue}</span><br>`;
            let total = 0
            params.forEach(({ seriesName, marker, value }) => {
              value = value || [0, 0];
              tooltip += `${marker} ${seriesName}<span style="float: right;margin-left:20px"><strong>${Number(value[1]).toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, " ")}</strong></span><br>`;
              total += Number(value[1]);
            });
            tooltip += `<br><b>Total</b><span style="float: right;margin-left:20px"><strong>${total.toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, " ")}</strong></span>`;
            return tooltip;
          }')) |>
    e_datazoom(x_index = 0, type = "slider", bottom = "3%") |>
    e_toolbox_feature(feature = "saveAsImage") |>
    e_toolbox_feature(feature = "dataView", readOnly = TRUE) |>
    e_x_axis(name="Week of sequencing results",
             nameLocation = "middle", nameGap= 25) |>
    e_grid(bottom = "17%") |>
    e_y_axis(name="Number of samples", nameLocation = 'middle',
             nameGap= 55, axisLabel=list(formatter = htmlwidgets::JS('function (value) { return value.toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, " ") ;}'))) |>
    e_title(text = "Number of the samples per variant per week",
            textStyle = list(fontSize = list(14)))

  return(echarts)
}

