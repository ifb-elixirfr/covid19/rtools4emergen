#' @title Create pie chart with sequencing type (public)
#' @author Thomas Denecker, \email{Thomas.Denecker@@france-bioinformatique.fr}
#' @author Chiara Antoinat, \email{chiara.antoinat@@gmail.com}
#' @description Create pie chart showing the number of samples per sequencing type (NGS or Sanger) in EMERGEN-DB. This interface is acessible without login (public).
#' @param base_url EMERGEN-DB URL. By default = 'https://emergen-db.france-bioinformatique.fr/fr/'
#' @return GGplot
#' @import httr
#' @import ggplot2
#' @import plotly
#' @import jsonlite
#' @export
public_stats_sequencing_type <- function(
  base_url = 'https://emergen-db.france-bioinformatique.fr/fr/') {
  message("API call from EMERGEN-DB")
  message("\tBase URL: ", base_url)

  # Entry point to get all the results in JSON format
  url = paste0(base_url, "api/stats/public/sequencing/type/")

  # Request JSON file with all results
  res = GET(url)

  if(res$status_code != 200) {
    message("\n\tError API\n")
    stop(paste("Status code:", res$status_code))
  }

  # Collecting data at JSON format
  json_object = jsonlite::prettify(rawToChar(res$content))

  # Conversion to the list format
  R_list_object = fromJSON(json_object)

  fig <- plot_ly(R_list_object$pie_data,
                 labels = ~name, values = ~value, type = 'pie')
  fig <- fig %>% layout(title = R_list_object$title,
                        xaxis = list(showgrid = FALSE,
                                     zeroline = FALSE, showticklabels = FALSE),
                        yaxis = list(showgrid = FALSE,
                                     zeroline = FALSE, showticklabels = FALSE))

  list_result = list()
  list_result$fig = fig
  list_result$api_values = R_list_object

  return(list_result)
}
