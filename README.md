# Rtools4Emergen

## Warning

This package is in construction. It aims at providing access to all the Application Programmatic Interfaces of the EMERGEN-DB databases, but it just started in Jan 2022, and it will be progressively populated with demos of different APIs. 

## Overview

**Language**

[![Made with R](https://img.shields.io/badge/Made%20with-R-blue)](https://www.r-project.org/)


**Questions**

[:speech_balloon: Ask a question](https://gitlab.com/ifb-elixirfr/covid19/rtools4emergen/-/issues/new)
[:book: Lisez les questions](https://gitlab.com/ifb-elixirfr/covid19/rtools4emergen/-/issues)
[:e-mail: Par mail](mailto:support-emergen-db@groupes.france-bioinformatique.fr)

**Contribution**

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code_of_conduct.md)


## Introduction

This R package provides tools to address queries to the EMERGEN-DB database (<https://emergen-db.france-bioinformatique.fr/>) via its REST API. 

EMERGEN-DB is the database that collects the metadata and genomic sequences produced by the French COVID-19 genomic surveillance project EMERGEN. 

## Installation

```R
library(devtools)
install_gitlab('ifb-elixirfr/covid19/rtools4emergen')
library(Rtools4Emergen)
```

## Package Manual

The documentation manual is provided here: [:book: Manual](https://gitlab.com/ifb-elixirfr/covid19/rtools4emergen/-/blob/main/manual/Rtools4Emergen_1.1.2.pdf)

## Contributors

### From IFB
* [Jaques Van Helden](https://gitlab.com/jvanheld) <a itemprop="sameAs" content="https://orcid.org/0000-0002-8799-8584" href="https://orcid.org/0000-0002-8799-8584" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
* [Thomas Denecker](https://gitlab.com/thomasdenecker) <a itemprop="sameAs" content="https://orcid.org/0000-0003-1421-7641" href="https://orcid.org/0000-0003-1421-7641" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
* [Chiara Antoinat](https://gitlab.com/ChiaraAntoinat) <a itemprop="sameAs" content="https://orcid.org/0000-0002-5456-2151" href="https://orcid.org/0000-0002-5456-2151" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
* [Imane Messak](https://gitlab.com/imanemessak) <a itemprop="sameAs" content="https://orcid.org/0000-0002-1654-6652" href="https://orcid.org/0000-0002-1654-6652" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
* [Anliat Mohamed](https://gitlab.com/anliatm) <a itemprop="sameAs" content="https://orcid.org/0000-0002-1105-8262" href="https://orcid.org/0000-0002-1105-8262" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>

## Contributing
Please, see the [CONTRIBUTING](CONTRIBUTING.md) file.

## Contributor Code of Conduct
Please note that this project is released with a [Contributor Code of Conduct](https://www.contributor-covenant.org/). By participating in this project you agree to abide by its terms. See [CODE_OF_CONDUCT](code_of_conduct.md) file.

## License

[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

EMERGEN-DB is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg

