---
title: "Activités de séquençage au sein du consortium EMERGEN"
output:
  html_document:
      includes:
        in_header: ./html/header_dashboard.html
params:
  si: NA
  emergenToken_params: NA
  workdir_params: NA
  base_url: NA
---

```{r setup, include=FALSE}
knitr::opts_knit$set(root.dir = params$workdir_params)
knitr::opts_chunk$set(echo = TRUE)
# base_url = 'https://emergen-db.france-bioinformatique.fr/fr/'
base_url = params$base_url

createCard <- function(value, icon, title, style){
  
  return(paste0('<div class="card mb-3 border-',style,' mx-auto shadow p-3 mb-5 bg-body rounded" style="max-width: 500px;">
      <div class="row g-0">
          <div class="col-md-4 align-self-center text-center">
      <span style="font-size: 95px; color: white; padding : 15px;" >
          <i class="', icon , ' text-',style,'"></i>
      </span>
          </div>
          <div class="col-md-8 align-self-center">
              <div class="card-body" style="padding: 0px 16px !important">
                  <span class="fs-1">',value,'</span><br>
                  <span class="text-muted mb-0 fs-5">',title,'</span>
              </div>
          </div>
      </div>
  </div>
  '))
}
createCard_tooltip <- function(value, icon, title, style, tooltiptile){
  
  return(paste0('<div class="card mb-3 border-',style,' mx-auto shadow p-3 mb-5 bg-body rounded" style="max-width: 500px;" data-toggle="tooltip" data-placement="top" title="',tooltiptile,'">
      <div class="row g-0">
          <div class="col-md-4 align-self-center text-center">
      <span style="font-size: 95px; color: white; padding : 15px;" >
          <i class="', icon , ' text-',style,'"></i>
      </span>
          </div>
          <div class="col-md-8 align-self-center">
              <div class="card-body" style="padding: 0px 16px !important">
                  <span class="fs-1">',value,'</span><br>
                  <span class="text-muted mb-0 fs-5">',title,'</span>
              </div>
          </div>
      </div>
  </div>
  '))
}
```

```{=html}
<br>
<div class="card mb-3">
  <div class="card-header bg-primary bg-gradient text-white">
    <span class="fs-5"><i class="fa-solid fa-circle-info"></i> Informations sur le rapport</span>
  </div>
  <div class="card-body">
  <p><b>Date de création</b>
```

`r paste0(format(Sys.time(), '%Y-%m-%d %H:%M'), " (",Sys.timezone() ,")")`

```{=html}
  </p>
  <p><b>Implémenté et généré par</b></p>
  <p>EMERGEN-DB team (IFB)</p>  
  <div class="d-grid gap-2 d-md-flex justify-content-md-end">
  <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#collapseInformation" aria-expanded="false" aria-controls="collapseInformation">
    <i class="fa-solid fa-circle-plus"></i> Plus d'informations
  </button>
</div>
<div class="collapse" id="collapseInformation">
  <div class="card card-body mt-2">
    <p> Ce rapport a été généré à l'aide du package Rtools4Emergen disponible sur <a href="https://gitlab.com/ifb-elixirfr/covid19/rtools4emergen" target="_blank">GitLab</a></p>
    <p><b> Informations sur la session </b></p>
```
```{r  echo = F}
params$si
```

```{=html}
  </div>
</div>
  </div>
</div>
```
```{=html}
<ul>
<li><i> Pour des questions concernant le projet EMERGEN ou l’activité de séquençage, veuillez contacter </i><a href = "mailto: emergen@santepubliquefrance.fr"><i>emergen@santepubliquefrance.fr</i></a></li>
<li><i>Pour signaler des problèmes techniques concernant le dashboard, veuillez contacter </i><a href = "mailto: support-emergen-db@groupes.france-bioinformatique.fr"><i>support-emergen-db@groupes.france-bioinformatique.fr</i></a></p></li>
</ul>
```

## Chiffres clés

```{r, echo=FALSE, message=FALSE, warning=FALSE}
url = paste0(base_url, "api/stats/public/overview/")
res= GET(url, add_headers(Authorization = paste("Token", params$emergenToken_params ,sep = " ")))
json_object = jsonlite::prettify(rawToChar(res$content))
R_list_object = fromJSON(json_object)
```

```{=html}
<div class="row">
  <div class="col-md-6">
```

`r knitr::asis_output(htmltools::htmlPreserve(createCard(R_list_object$count_results, "fa-solid fa-square-poll-vertical", "résultats de séquençage déposés sur EMERGEN DB", "primary")))`

```{=html}
</div>
  <div class="col-md-6">
```

`r knitr::asis_output(htmltools::htmlPreserve(createCard(R_list_object$last_import, "fas fa-calendar-alt", "Dernière soumission sur EMERGEN-DB", "dark")))`

```{=html}
</div>
  </div>
```
```{=html}
<div class="row">
  <div class="col-md-6">
```

`r knitr::asis_output(htmltools::htmlPreserve(createCard_tooltip(R_list_object$count_fasta_files, "fa-solid fa-file", "fichiers FASTA déposés sur EMERGEN-DB", "success", "Nombre de séquences déposées sur EMERGEN-DB")))`

```{=html}
</div>
  <div class="col-md-6">
```

`r knitr::asis_output(htmltools::htmlPreserve(createCard_tooltip(R_list_object$count_percent_fasta, "fa-solid fa-chart-pie", "de fichiers FASTA exploitables", "danger", "Pourcentege des séquences non vides dans EMERGEN-DB")))`

```{=html}
</div>
  </div>
```
```{=html}
<div class="row">
  <div class="col-md-6">
```

`r knitr::asis_output(htmltools::htmlPreserve(createCard(R_list_object$count_GISAID_sub_accepted, "fa-solid fa-square-arrow-up-right", "Soumissions à GISAID effectuées par l’IFB", "warning")))`

```{=html}
</div>
  <div class="col-md-6">
<div class="card mb-3 border-secondary mx-auto shadow p-3 mb-5 bg-body rounded" style="max-width: 500px;">
      <div class="row g-0">
          <div class="col-md-4 align-self-center text-center">
      <span style="font-size: 95px; color: white; padding : 15px;" >
          <i class="fa-solid fa-square-arrow-up-right text-secondary"></i>
      </span>
          </div>
          <div class="col-md-8 align-self-center">
              <div class="card-body" style="padding: 0px 16px !important">
                  <h4 class="text-muted mb-0"><a href="https://gisaid.org/submission-tracker-global/" target="_blank">Données EMERGEN-DB soumises à GISAID</a> </h4>
              </div>
          </div>
      </div>
  </div>
</div>
  </div>
```

### Informations importantes

-   Technologies de séquençage sélectionnées : NGS (complet et partiel) et Sanger (partiel).

## Activité globale de séquençage

Les activités du consortium EMERGEN sont réparties entre 5 types de plateformes de séquençage, habilitées à faire du séquençage pour différentes indications :

-   **CNR** : Centre National de Référence Virus des infections respiratoires (2 plateformes) ;
-   **ANRS\|MIE** : Réseau de laboratoires hospitaliers coordonné par l’ANRS|MIE ;
-   **CNR-LE** : CNR - Laboratoires Experts pour l’appui au séquençage du SARS-CoV-2 (2 plateformes) ; fin de leur mandat au 31/12/2022 (réintégrés dans le réseau ANRS|MIE depuis) ;
-   **AMI** : Laboratoires de biologie médicale privés sélectionnés par Santé publique France après appel à manifestation d’intérêt (AMI) ; fin de leur mandat au 31/12/2022 ;
-   **LBM-ARS** : Laboratoires de biologie médicale privés conventionnés avec les Agences Régionales de Santé.

Pour informations sur les indications se référer au dossier du consortium Emergen [Santé Publique France](https://www.santepubliquefrance.fr/content/download/356335/3079336){target="_blank"}.

```{=html}
<h3 class="card-header mb-3 text-light bg-primary">Activité de séquençage du consortium EMERGEN</h3>
```

**Figure 1 -- Activité de séquençage du consortium EMERGEN, par type de laboratoire, par semaine de résultat**

```{=html}
<b style='color:red;'> NOTE: les données d’activité des deux dernières semaines ne sont pas consolidées.</b><br><br>
```

```{r, echo=FALSE, message=FALSE, warning=FALSE}
plot_sequencing_activity(base_url=base_url, emergenToken=params$emergenToken_params)
```

```{=html}
<h3 class="card-header mb-3 text-light bg-primary">Délais médians de séquençage par type de laboratoire</h3>
```

**Figure 2 -- Délais médians de séquençage par type de laboratoires (plateformes CNR, platformes CNR-LE, plateformes AMI, réseau ANRS\|MIE et réseau LBM-ARS) par semaine de réception du prélèvement**

```{=html}
<b style='color:red;'> NOTE : les données d’activité des deux dernières semaines ne sont pas consolidées.</b><br><br>
```

```{r, echo=FALSE, message=FALSE, warning=FALSE}
plot_delais_medians(base_url=base_url, emergenToken = params$emergenToken_params)
```

Le délai de séquençage correspond à la différence entre la date de réception et la date de résultat du séquençage d'un prélèvement. Ces délais oscillent normalement entre 6 et 9 jours, avec des pics éventuels qui correspondent normalement à des périodes où l'activité de séquençage a été plus intense (voir Figure 1). Dans le slider, la ligne bleue correspond à la moyenne des délais médians.

```{=html}
<h3 class="card-header mb-3 text-light bg-primary">Activité selon l'indication des séquences produites</h3>
```

**Figure 3 -- Nombre de séquences produites par date de prélèvement et par indication**

```{=html}
<b style='color:red;'> NOTE : les données d’activité des deux dernières semaines ne sont pas consolidées.</b><br><br>
```

```{r, echo=FALSE, message=FALSE, warning=FALSE}
plot_sequences_by_indications(base_url=base_url, emergenToken = params$emergenToken_params)
```

## Activité par semaine

```{=html}
<h3 class="card-header mb-3 text-light bg-primary">Capacité de séquençage en NGS par type de laboratoire et par semaine</h3>
```

**Figure 4 -- Utilisation des capacités NGS (en %) par type de plateforme et par semaine**

```{=html}
<b style='color:red;'> NOTE : les données d’activité des deux dernières semaines ne sont pas consolidées.</b><br><br>
```

```{r, echo=FALSE, message=FALSE}
dataTemp = get_sequencing_capacity_data(base_url=base_url, emergenToken = params$emergenToken_params)
```

```{=html}
<div class="row justify-content-md-center">
  <div class="col-lg-3">
```

`r plot_gauge_chart_capacity(dataTemp, 'CNR', 'CNR', FALSE)`

```{=html}
</div>
  <div class="col-lg-3">
```

`r plot_gauge_chart_capacity(dataTemp, 'CNR-LE', 'CNR-LE', FALSE)`

```{=html}
  </div>
  <div class="col-lg-3">
```

`r plot_gauge_chart_capacity(dataTemp,'ANRS|MIE', 'ANRS', FALSE)`

```{=html}
   </div>
   </div>
   <div class="row justify-content-md-center">
   <div class="col-lg-3">
```

`r plot_gauge_chart_capacity(dataTemp, 'LBM-ARS', 'LBM', FALSE)`

```{=html}
  </div>
  <div class="col-lg-3">
```
`r plot_gauge_chart_capacity(dataTemp, 'AMI', 'AMI', FALSE)`
```{=html}
  </div>
</div>
```

```{r echo=FALSE, message=FALSE, warning=FALSE}
plot_gauge_chart_capacity_consortium(dataTemp)
```

Les capacités NGS génome complet ont été déclarées par les laboratoires du consortium, et elles sont agrégées ici par type de laboratoire. Elles correspondent à une activité de routine, qui peut être augmentée pour certaines plateformes dans le cadre d'opérations ponctuelles.

```{=html}
<h3 class="card-header mb-3 text-light bg-primary">Nombre de séquences produites par type de laboratoires</h3>
```

**Figure 5 -- Nombre de séquences produites par type de plateforme et par semaine et capacités maximales NGS hebdomadaires déclarées**

```{=html}
<b style='color:red;'> NOTE : les données d’activité des deux dernières semaines ne sont pas consolidées.</b><br><br>
```

```{r, echo=FALSE, message=FALSE, warning=FALSE}
plot_capacity_per_week(dataTemp)
```

```{=html}
<h3 class="card-header mb-3 text-light bg-primary">Activité de séquençage par département</h3>
```

**Carte 1: Nombre de prélèvements séquencés par département d'origine du patient et par semaine de prélèvement**

```{=html}
<b style='color:red;'> NOTE : les données d’activité des deux dernières semaines ne sont pas consolidées.</b><br><br>
```

```{r, echo=FALSE, message=FALSE, warning=FALSE}
plot_activity_by_departement(base_url=base_url, emergenToken = params$emergenToken_params)
```

Le chiffre affiché correspond au nombre de prélèvements envoyés par les laboratoires de première intention aux différentes plateformes de séquençage. La détermination de l'origine du patient est faite avec le critère « département de résidence » par défaut, remplacé par département du laboratoire préleveur ou département du laboratoire expéditeur en son absence.

```{=html}
<h3 class="card-header mb-3 text-light bg-primary">Activité de séquençage par région</h3>
```

**Carte 2: Nombre de prélèvements séquencés par région d'origine du patient et par semaine de prélèvement**

```{=html}
<b style='color:red;'> NOTE : les données d’activité des deux dernières semaines ne sont pas consolidées.</b><br><br>
```

```{r, echo=FALSE, message=FALSE, warning=FALSE}
plot_activity_by_region(base_url=base_url, emergenToken = params$emergenToken_params)
```

Le chiffre affiché correspond au nombre de prélèvements envoyés par les laboratoires de première intention aux différentes plateformes de séquençage. La détermination de l'origine du patient est faite avec le critère « région de résidence » par défaut, remplacé par région du laboratoire préleveur ou région du laboratoire expéditeur en son absence.
