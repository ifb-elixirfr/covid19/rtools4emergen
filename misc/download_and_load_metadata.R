#### Download and load all the metadata from EMERGEN-DB ####


################################################################################
###                                Library                                   ###
################################################################################

library(httr)

################################################################################
###                                 MAIN                                     ###
################################################################################

# A ADAPTER POUR LE RENDRE FONCTIONNEL

#===============================================================================
# Récupération des fichiers TSV
#===============================================================================

# base_url = 'https://emergen-db.france-bioinformatique.fr/'
#
# # Création de variables
# url = paste0(base_url,"api/request/TSV/all/results/")
# # Information : EMERGEN_TOKEN is not a real token
# token = Sys.getenv("EMERGEN_TOKEN")
#
# # Requête du fichier TSV avec tous les résultat
# # res = GET(url, add_headers(Authorization = paste("Token", token, sep = " ")))
#
# # Récupération de l'ID de la tache asynchrone
# results = unlist(content(res))
# task_ID = results["Task_id"]
# url = paste0(base_url , "api/task/status/", task_ID,"/",
#              collapse = "")
#
# # Téléchargement du fichier lorsque la tache est terminée
# # Tentative toutes les 30 secondes pour l'exemple
# # Mettre un temps plus long pour une grosse base de données
#
# count = 1
# repeat {
#   message("Iteration ", count)
#   res = GET(url, add_headers(Authorization = paste("Token", token, sep = " ")))
#   results = unlist(content(res))
#
#   if (results["Status"] == "SUCCESS") {
#     message("Downloading the result archive")
#     url =  paste0(base_url,"api/get/TSV/all/results/")
#     res2 = GET(url, add_headers(Authorization = paste("Token", token, sep = " ")),
#                write_disk("export_db_tsv.tar.gz", overwrite = TRUE))
#     break
#   } else if (count > 100) {
#     print('TROP LONG')
#     break
#   } else {
#     count = count + 1
#     Sys.sleep(10)
#   }
# }
#
# #===============================================================================
# # Lecture des fichiers TSV
# #===============================================================================
#
# # Extraction tar.gz
# untar("export_db_tsv.tar.gz")
#
# # Lecture
# results_all <- read.csv("database_export/export_results.tsv", sep ="\t")
# read_me <- read.csv("database_export/README.tsv", sep ="\t")
# tsv_info <- read.csv("database_export/export_excel_info.tsv", sep ="\t")
#
# #### Check the results ####
# nrow(results_all)
# system("wc -l database_export/export_results.tsv")
# ## PROBLEM: there are more lines in the TSV file than in the data.frame. This is due to the presence of \n characters in te address field
#
# results_all_2 <- read.csv("database_export/export_results.tsv", sep ="\t", quote=NULL)
# nrow(results_all_2)

